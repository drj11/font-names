# Names

Acyclic
Archangel
Avimore
Actinite / Actinium
Agar / Agar agar
Agaric
Avondale
Arecibo

Abducto (Abduct taken)
Acetic
Adjunct
Adjutant
Adjuvant
Advance
Aeolian
Aerobic
Aerobix
Aerosol
Agamemnon
Agonize
Agrimony
Airbase
Airfare
Airflow
Airlift
Airplay
Airship
Airport
Airline
Airmail
Airshow
Airtime
Aileron
Alchemy
Alfred
Aliquot
Alkaline (taken, it's quite a good script)
Allegro
Ambient
Ancient
Antigone
Antique
Apeshit
Archimedes
Archive
Arquebus
Article
Artwork
Ascribe
Aseptic
Aspartic
Aspire
Astron
Auger
Athena (taken, ugly and ancient)
Avena
Avignon
Azimuth

Adorbs / Adorbs Babes / Adorbs Yay! / Adorbs Much

Creative - a trace of Courier from the BBC Micro books

Acourier circle
Acirc
Acirci round via ring
Acringu
Aring
Arou

correspondence messenger airmail
carillion
augeron

# For a dot matrix font

point
dot
punkt
pin
wire
jot
iota
mote
spot
period
tittle
pika

aguadot
aquadot
aquajot

step
sixstep
stepika

matrix
domapri
impadot
jotodot
dotadot
dotdotdot
pseudot
sudot
fauxdot
magidot
digidot
Dorothy
peridot
dottie
Wire Strike

- arpwynt (ar pwynt = at a point)
- apoint (french: to point)
- pikaboo (slovenian pika = point) (taken)
- pikadot / pikajot


# END
